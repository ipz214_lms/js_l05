const calc = document.querySelector(".calculator");
const output = document.querySelector(".output");
calc.addEventListener("click", function (e) {
  if (e.target.dataset.op || e.target.hasAttribute("data-num")) {
    output.textContent += e.target.value;
  }
  if (e.target.hasAttribute("data-equal"))
    output.textContent = eval(output.textContent);
  if(e.target.hasAttribute('data-all-clear')) {
    output.textContent = '';
  }
});
